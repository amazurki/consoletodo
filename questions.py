MENU_ENTRIES = [
    "See existing task",
    "Add task",
    "Remove task",
    "Edit task",
    "Get task from external source",
    "Export  my task",
]

MENU_QUESTION = {
    "type": "list",
    "name": "menu",
    "message": "What do you want to do?",
    "choices": MENU_ENTRIES,
}
