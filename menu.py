from PyInquirer import style_from_dict, prompt

from questions import MENU_QUESTION


def main():
    print("Welcome to XXXXXXXXX.")
    menu()


def add_task():
    print("Adding task")


def remove_task():
    print("Removing task")


def menu():

    answer = prompt(MENU_QUESTION)["menu"]
    if answer == "Add task":
        add_task()


if __name__ == "__main__":
    main()
